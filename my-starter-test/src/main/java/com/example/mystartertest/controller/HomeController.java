package com.example.mystartertest.controller;


import com.zzj.demo.StringUtil;
import com.zzj.demo.bean.Info;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class HomeController {


    @Resource
    private StringUtil stringUtil;

    @Resource
    private Info info;

    /**
     * 测试StringUtil工具
     * @return
     */
    @GetMapping
    public Object list(){


        return stringUtil.strSplit("1,2,3,4",",");
    }

    /**
     * 测试starter配置
     * @return
     */
    @GetMapping("/config")
    public Object config(){

        return info;
    }
}
