package com.zzj.demo.autoConfig;

import com.zzj.demo.StringUtil;
import com.zzj.demo.bean.Info;
import com.zzj.demo.bean.InfoProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(InfoProperties.class)
public class MyAutoConfigure {


    @Bean
    public StringUtil stringUtil(){

        return new StringUtil();
    }

    @Bean
    public Info info(InfoProperties properties){

        Info info = new Info();
        info.setName(properties.getName());
        info.setAge(properties.getAge());
        info.setSex(properties.getSex());
        return info;
    }
}
