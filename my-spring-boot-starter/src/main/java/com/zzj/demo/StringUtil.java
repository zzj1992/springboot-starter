package com.zzj.demo;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class StringUtil {


    /**
     * 字符串分割成集合
     * @param s
     * @param regex
     * @return
     */
    public List<String> strSplit(String s,String regex){


        if(s==null||s.length()==0){

            return Collections.emptyList();
        }

        String[] split = s.split(regex);

        return Arrays.asList(split);
    }

}
